package ito.poo.app;

import java.util.Scanner;


public class Practica2 {

	static Scanner input= new Scanner(System.in);
	//=============================================
	// Este m�todo debe leer desde el teclado
	// cada uno de los elementos de un arreglo
	// que es pasado como argumento.
	//=============================================
	static void leerArreglo (int [] a) {
		System.out.println("Inserte el dia para entregar las tareas:");
		for(int i=0; i<a.length; i++){
		System.out.print("Tarea "+(i+1)+": ");
		a[i]=input.nextInt();
		}
		}
	//=============================================
	// Este m�todo debe retornar el n�mero de tareas
	// que han vencido en funci�n del d�a (hoy) pasado
	// como argumento.
	// =============================================
	static int cantidaTareasVencidas(int [] a , int hoy) {
		int i=0;
		for(int x=0; x<a.length; x++){
		if(a[x]<=hoy){
		i++;
		}
		}
		return i;
		}
	// =============================================
	// Este m�todo debe retornar la lista de tareas que 
	// han vencido al d�a (hoy).
	// =============================================
	static int [] tareasVencidas(int [] a, int hoy) {
		int z= cantidaTareasVencidas(a, hoy);
		int[] lv= new int[z];
		for(int i=0; i<a.length; i++){
		if(a[i]<=hoy){
		lv[i]=a[i];
		}
		}
		return lv;
		}
	static int cantidadTareasProximas(int [] a, int hoy) {
		int c=0, b=0;
		b=hoy+5;
		for(int i=0; i<a.length; i++){
		if(a[i]>hoy && a[i]<b){
				c++;
			}
		}
		return c;
	}	
	static int [] tareasProximas(int [] a, int hoy) {
		int y=0, x=0, w=0;
		y=cantidadTareasProximas(a, hoy);
		int[] lv= new int[y];
		x=hoy+5;
		for(int i=0; i<a.length; i++){
		if(a[i]>hoy && a[i]<x){
		lv[w]=a[i];
		w++;
		}
		}
		return lv;
		}
	static int cantidadTareasAReprogramar(int [] a, int hoy) {
		int b=0;
		for(int i=0; i<a.length; i++){
		if(a[i]>(hoy+5)){
		b++;
		}
		}
		return b;
		}
	static int [] tareasAReprogramar(int [] a, int hoy) {
		int v=0, n=0;
		v=cantidadTareasAReprogramar(a, hoy);
		int [] lv= new int[v];
		for(int i=0; i<a.length; i++){
		if(a[i]>(hoy+5)) {
		lv[n]=a[i];
		n++;
		}
		}
		return lv;
		}
	static void imprimeResultados(int [] a) {
		if(a.length!=0){
		System.out.print("[");
		for(int i=0; i<a.length; i++){
		System.out.print(a[i]+",");
		}
		System.out.println("]");
		}else {
		System.out.println("No hay ninguna tarea");
		}
		}
		static void run() {
		int a[] = new int[6];
		int x[] = {};
		int dia = 3;
		leerArreglo(a);
		x=tareasVencidas(a, dia);
		System.out.print("Las tareas que estan vencidas son: ");
		imprimeResultados(x);
		x=tareasProximas(a, dia);
		System.out.print("Las tareas proximas a entregar son: ");
		imprimeResultados(x);
		x=tareasAReprogramar(a, dia);
		System.out.print("Las tareas a reprogramar otro dia son: ");
		imprimeResultados(x);
		}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        run();
	}

}
